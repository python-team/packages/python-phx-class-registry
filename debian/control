Source: python-phx-class-registry
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Emanuele Rocca <ema@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-all,
               python3-setuptools,
               python3-sphinx,
               python3-sphinx-rtd-theme
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://github.com/todofixthis/class-registry
Vcs-Git: https://salsa.debian.org/python-team/packages/python-phx-class-registry.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-phx-class-registry

Package: python3-phx-class-registry
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Provides: ${python3:Provides}
Description: Module to define global factories and service registries
 At the intersection of the Registry and Factory patterns lies the
 ClassRegistry.
 .
 This module allows to:
 .
  * Define global factories that generate new class instances based on
    configurable keys.
  * Seamlessly create powerful service registries.
  * Integrate with setuptools's entry_points system to make registries
    extensible by 3rd-party libraries.
 .
 This package provides the Python 3 version of the class_registry module.

Package: python-phx-class-registry-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Description: Module to define global factories and service registries - Documentation
 At the intersection of the Registry and Factory patterns lies the
 ClassRegistry.
 .
 This module allows to:
 .
  * Define global factories that generate new class instances based on
    configurable keys.
  * Seamlessly create powerful service registries.
  * Integrate with setuptools's entry_points system to make registries
    extensible by 3rd-party libraries.
 .
 This package provides the documentation for the class_registry module.
