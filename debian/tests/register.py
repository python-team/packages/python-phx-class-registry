from class_registry import ClassRegistry

pokedex = ClassRegistry()

@pokedex.register('fire')
class Charizard(object):
    pass

@pokedex.register('grass')
class Bulbasaur(object):
    pass

fighter1 = pokedex['fire']
assert type(fighter1) is Charizard

fighter2 = pokedex['grass']
assert type(fighter2) is Bulbasaur
